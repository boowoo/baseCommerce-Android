package boo.basecommerce_android;

import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

import boo.basecommerce_android.adapter.CommercePagerAdapter;
import boo.basecommerce_android.fragment.BlankFragment;

public class MainActivity extends FragmentActivity implements BlankFragment.OnFragmentInteractionListener{

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initLayout();
        initLayoutEvent();
    }

    private void initLayout(){
        viewPager = (ViewPager)findViewById(R.id.view_pager);
    }

    private void initLayoutEvent(){
        viewPager.setAdapter(new CommercePagerAdapter(getSupportFragmentManager()));
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
