package boo.basecommerce_android.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import boo.basecommerce_android.fragment.BlankFragment;

/**
 * Created by jins on 2016-07-29.
 */

public class CommercePagerAdapter extends FragmentPagerAdapter {

    public CommercePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment;

        switch (position){
            case 0:
                fragment = BlankFragment.newInstance("", "");
                break;
            case 1:
                fragment = BlankFragment.newInstance("", "");
                break;
            case 2:
                fragment = BlankFragment.newInstance("", "");
                break;
            case 3:
                fragment = BlankFragment.newInstance("", "");
                break;
            default:
                fragment = BlankFragment.newInstance("", "");
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
